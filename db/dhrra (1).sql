-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2020 at 04:19 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dhrra`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `aname` varchar(255) NOT NULL,
  `aphone` varchar(255) NOT NULL,
  `aaddress` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `aname`, `aphone`, `aaddress`, `username`, `password`, `email`) VALUES
(1, 'admin', '8778240963', 'test1', 'admin', 'admin', 'satheshram6@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `cname` varchar(100) NOT NULL,
  `cphone` varchar(100) NOT NULL,
  `cemail` varchar(100) NOT NULL,
  `cmessage` varchar(100) NOT NULL,
  `caddress` varchar(255) NOT NULL,
  `cdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `fld_delete` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `donate`
--

CREATE TABLE `donate` (
  `id` int(11) NOT NULL,
  `dname` varchar(100) NOT NULL,
  `dphone` varchar(100) NOT NULL,
  `demail` varchar(100) NOT NULL,
  `daddress` varchar(100) NOT NULL,
  `dpaid` varchar(100) NOT NULL,
  `dpan` varchar(100) NOT NULL,
  `status` int(100) NOT NULL,
  `ddate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `fld_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `donate_monthly`
--

CREATE TABLE `donate_monthly` (
  `id` int(11) NOT NULL,
  `dmname` varchar(100) NOT NULL,
  `dmphone` varchar(100) NOT NULL,
  `dmemail` varchar(100) NOT NULL,
  `dmaddress` varchar(100) NOT NULL,
  `dmpaid` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `dmdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `dmpan` varchar(100) NOT NULL,
  `dmfeedback` varchar(255) NOT NULL,
  `fld_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `pillar_category_id` int(11) NOT NULL,
  `gname` varchar(100) NOT NULL,
  `filetype` varchar(100) NOT NULL,
  `gimg` varchar(255) NOT NULL,
  `gdescription` text NOT NULL,
  `gdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `status` int(100) NOT NULL,
  `fld_g_delete` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `galleryid` int(11) NOT NULL,
  `images1` varchar(100) NOT NULL,
  `idate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `status1` int(11) NOT NULL,
  `fld_i_delete` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pillar`
--

CREATE TABLE `pillar` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `project` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `gallery` varchar(255) NOT NULL,
  `status` int(100) NOT NULL,
  `fld_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `press`
--

CREATE TABLE `press` (
  `id` int(11) NOT NULL,
  `pillar_category_id` int(11) NOT NULL,
  `prname` varchar(100) NOT NULL,
  `prtitle` varchar(255) NOT NULL,
  `prdescription` text NOT NULL,
  `primage` varchar(100) NOT NULL,
  `status` int(100) NOT NULL,
  `prdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `fld_pr_delete` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pressgallery`
--

CREATE TABLE `pressgallery` (
  `id` int(11) NOT NULL,
  `pgid` int(11) NOT NULL,
  `pgimage` varchar(255) NOT NULL,
  `pgdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `status` int(11) NOT NULL,
  `fld_p_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `pillar_category_id` int(11) NOT NULL,
  `pname` varchar(100) NOT NULL,
  `pgallery` varchar(100) NOT NULL,
  `pdescription` text NOT NULL,
  `status` int(100) NOT NULL,
  `pdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `fld_p_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL,
  `puname` varchar(100) NOT NULL,
  `puimage` varchar(255) NOT NULL,
  `status` int(100) NOT NULL,
  `pudate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `pu_delete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `publication_management`
--

CREATE TABLE `publication_management` (
  `id` int(11) NOT NULL,
  `pillar_category_id` int(11) NOT NULL,
  `pumname` varchar(100) NOT NULL,
  `pumpuname` varchar(100) NOT NULL,
  `pumdescription` text NOT NULL,
  `pumimage` varchar(255) NOT NULL,
  `pumattach` varchar(100) NOT NULL,
  `pumdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `status` int(100) NOT NULL,
  `fld_pum_delete` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `pillar_category_id` int(11) NOT NULL,
  `rname` varchar(100) NOT NULL,
  `rdescription` text NOT NULL,
  `rimage` varchar(100) NOT NULL,
  `rpdf` varchar(100) NOT NULL,
  `status` int(100) NOT NULL,
  `rdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `fld_r_delete` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `pillar_category_id` int(11) NOT NULL,
  `vname` varchar(100) NOT NULL,
  `vdescription` text NOT NULL,
  `vurl` text NOT NULL,
  `vvideo` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `vdate` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `fld_v_delete` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `volunteer`
--

CREATE TABLE `volunteer` (
  `id` int(11) NOT NULL,
  `v1name` varchar(100) NOT NULL,
  `v1email` varchar(100) NOT NULL,
  `v1phone` varchar(100) NOT NULL,
  `v1subject` varchar(100) NOT NULL,
  `v1education` varchar(100) NOT NULL,
  `v1experience` varchar(100) NOT NULL,
  `v1message` varchar(100) NOT NULL,
  `v1date` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `v1gender` varchar(199) NOT NULL,
  `fld_delete` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donate`
--
ALTER TABLE `donate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donate_monthly`
--
ALTER TABLE `donate_monthly`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pillar`
--
ALTER TABLE `pillar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `press`
--
ALTER TABLE `press`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pressgallery`
--
ALTER TABLE `pressgallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `publication_management`
--
ALTER TABLE `publication_management`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `donate`
--
ALTER TABLE `donate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `donate_monthly`
--
ALTER TABLE `donate_monthly`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pillar`
--
ALTER TABLE `pillar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `press`
--
ALTER TABLE `press`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pressgallery`
--
ALTER TABLE `pressgallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `publication_management`
--
ALTER TABLE `publication_management`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `volunteer`
--
ALTER TABLE `volunteer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
