<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['username']))
{
echo "<script>window.location.href='index.php'</script>";

}
else
{

include"header.php";

include("db.php");

?>

<div class="pcoded-content">
    <div class="pcoded-inner-content">

        <div class="main-body">
            <div class="page-wrapper">

                <div class="page-header card">
                    <div class="card-block">
                        <h5 class="m-b-10">Classes Management</h5>
                        
                        <ul class="breadcrumb-title b-t-default p-t-10">
                            <li class="breadcrumb-item">
                                <a href="dashboard.php"> <i class="ti-home"></i> </a>
                            </li>
                            <li class="breadcrumb-item"><a href="web.php">Classes</a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!"> Add Classes</a>
                            </li>
                        </ul>
                    </div>
                </div>


                <div class="page-body">
                    <div class="row">
                        <div class="col-sm-12">

                            <div class="card">
                                <div class="card-header">
                                    <h5>Add Classes</h5>
                                    <!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
                                </div>
                                <div class="card-block">
                                    <!-- <h4 class="sub-title">Basic Inputs</h4> -->
                                    <form method="post" id="uploadForm" enctype="multipart/form-data">

                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Package Category <b style="color: red">*</b></label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="package_id" required="">
                                                    <option value="">Select Package</option>
                                                    <?php
  $pillarcat=mysqli_query($conn,"SELECT * FROM package WHERE status=0 AND del=0");
  while ($rowcat=mysqli_fetch_array($pillarcat)) {
    echo "<option value='".$rowcat['id']."'>".$rowcat['planname']."</option>";
  }
  ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Title <b style="color: red">*</b></label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="title" placeholder="Title" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label">Description <b style="color: red">*</b></label>
                                            <div class="col-sm-10">
                                                
                                                <span id="error_productdescription" style="color:red;align:center;font-weight:900;font-size:16px;"></span>

                                                
                                                <textarea class="form-control" id="des" name="des" required="" maxlength="1200"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label"> Classes Video</label>
                                            <div class="col-sm-10">
                                                <input type="file" name="file" id="fileInput">
                                                <br>
                                                <div class="progress">
                                                    <div class="progress-bar"></div>
                                                </div>

                                                <div id="uploadStatus"></div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group row">
                                            <label class="col-sm-2 col-form-label"> Images</label>
                                            <div class="col-sm-10">
                                                <input type="file" class="form-control" name="image" size="100" required="">
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group row">
                                            <div class="col-sm-4">
                                            </div>
                                            <div class="col-sm-4">
                                                <input type="submit" class="form-control btn btn-info" name="pillarsub" value="Submit" style="border-radius: 12px;">
                                            </div>
                                            <div class="col-sm-4">
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

<script>
    CKEDITOR.replace('editor1');
    /* $("form").submit( function(e) {
        var messageLength = CKEDITOR.instances['editor'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            alert( 'Please enter a message' );
            e.preventDefault();
        }
    }); */
</script>

<script type="text/javascript">
    var messageLength = CKEDITOR.instances['editor1'].getData().replace(/<[^>]*>/gi, '').length;

    if (!messageLength) {
        document.getElementById('error_productdescription').innerHTML = "Please Enter Product Description";
        return false;
    } else {
        document.getElementById('error_productdescription').innerHTML = "";
    }
</script>

<script>
    $(document).ready(function() {
        // File upload via Ajax
        $("#uploadForm").on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = ((evt.loaded / evt.total) * 100);
                            $(".progress-bar").width(percentComplete + '%');
                            $(".progress-bar").html(percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                type: 'POST',
                url: 'up1.php',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $(".progress-bar").width('0%');
                    $('#uploadStatus').html('<img src="total/l.gif" width="20%" />');
                },
                error: function() {
                    $('#uploadStatus').html('<p style="color:#EA4335;">File upload failed, please try again.</p>');
                },
                success: function(resp) {
                    alert(resp);
                    console.log(resp);
                    // if (resp == 'ok') {
                    //   alert();
                        $('#uploadForm')[0].reset();
                        $('#uploadStatus').html('<p style="color:#28A74B;">File has uploaded successfully!</p>');
                        // window.location.href='web.php';
                    // } else if (resp == 'err') {
                    //     $('#uploadStatus').html('<p style="color:#EA4335;">Please select a valid file to upload.</p>');
                    // }
                }
            });
        });

        // File type validation
        $("#fileInput").change(function() {
            var allowedTypes = ['application/pdf', 'application/msword', 'application/vnd.ms-office', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'image/jpeg', 'image/png', 'image/jpg', 'image/gif', 'video/mp4'];
            var file = this.files[0];
            var fileType = file.type;
            if (!allowedTypes.includes(fileType)) {
                alert('Please select a valid file (PDF/DOC/DOCX/JPEG/JPG/PNG/GIF/MP4).');
                $("#fileInput").val('');
                return false;
            }
        });
    });
</script>


<?php
include"footer.php";
}
?>