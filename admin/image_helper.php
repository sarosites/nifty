<?php 

if ( ! function_exists('resizeImage'))
{
    function resizeImage($sourceImage,$maxWidth, $maxHeight)
    {
        $CI =& get_instance();
        $config['source_image'] = $sourceImage;
        $config['maintain_ratio'] = FALSE;
        $config['width'] = $maxWidth;
        $config['height'] = $maxHeight;
        $CI->load->library('image_lib');
        $CI->image_lib->initialize($config);
        $CI->image_lib->resize();
        
        return true;
    }   
}
if ( ! function_exists('upload_data'))
{
    function upload_data($do_upload_name,$uploadPath,$allowedTypes,$maxSize,$file,$maxWidth="", $maxHeight="",$resizedWidth="",$resizedHeight="")
    {
        unset($_FILES['files']);
        $number_of_files=sizeof($file['tmp_name']);
        $CI =& get_instance();
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = $allowedTypes;
        $config['max_size'] = $maxSize;
        if($maxWidth!="" && $maxHeight!="")
        {
            $config['min_width'] = $maxWidth;
            $config['min_height'] = $maxHeight;
        }
        $CI->load->library('upload', $config);
        for ($i = 0; $i < $number_of_files; $i++)
        {
            if($file['error'][$i]==0)
            {
                $_FILES['files']['name']     = $file['name'][$i];
                $_FILES['files']['type']     = $file['type'][$i];
                $_FILES['files']['tmp_name'] = $file['tmp_name'][$i];
                $_FILES['files']['error']    = $file['error'][$i];
                $_FILES['files']['size']     = $file['size'][$i];
                //$config['file_name']=rand();
                $CI->upload->initialize($config);
                 if ( ! $CI->upload->do_upload($do_upload_name))
                {
                    $data2 = array('error' => $CI->upload->display_errors());
                }
                else
                {
                    $data2 = array('upload_data' => $CI->upload->data());
                    if($resizedWidth!="" && $resizedHeight!="")
                    {
                        resizeImage($CI->upload->upload_path.$CI->upload->file_name,$resizedWidth, $resizedHeight);
                    }
                }
              return $data2;
            }
        }
    }
}
if ( ! function_exists('upload_file_data'))
{
    function upload_file_data($do_upload_name,$uploadPath,$allowedTypes,$maxSize="",$file,$maxWidth="", $maxHeight="",$file_name="",$old_file="")
    {
        $data=array();
        unset($_FILES['files']);
        $number_of_files=sizeof($file['tmp_name']);
       $CI =& get_instance();
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = $allowedTypes;
        if($maxSize!="")
        {
            $config['max_size'] = $maxSize;
        }
        $CI->load->library('upload', $config);
        for ($i = 0; $i < $number_of_files; $i++)
        {
            if($file['error'][$i]==0)
            {
                $_FILES['files']['name']     = $file['name'][$i];
                $_FILES['files']['type']     = $file['type'][$i];
                $_FILES['files']['tmp_name'] = $file['tmp_name'][$i];
                $_FILES['files']['error']    = $file['error'][$i];
                $_FILES['files']['size']     = $file['size'][$i];
                if($old_file=="")
                {
                    if($file_name=="")
                    {
                        $config['file_name']=rand();
                    }
                    else
                    {
                        $config['file_name']=$file_name;
                    }
                }
                $CI->upload->initialize($config);
                 if ( ! $CI->upload->do_upload($do_upload_name))
                {
                    $data = array('error' => $CI->upload->display_errors());
                }
                else
                {
                    $data = array('upload_data' => $CI->upload->data());
                    if($maxWidth!="" && $maxHeight!="")
                    {
                        resizeImage($CI->upload->upload_path.$CI->upload->file_name,$maxWidth, $maxHeight);
                    }
                }
            }
        }
        return $data;
    }
}