importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-messaging.js');

var firebaseConfig = {
        apiKey: "AIzaSyBGTGe7FSHo1DnK-VaZCcrwN_Shcsxiab8",
        authDomain: "https://console.firebase.google.com/project/nifty-master-464ff/",
        databaseURL: "https://nifty-master-464ff-default-rtdb.firebaseio.com/",
        projectId: "nifty-master-464ff",
        storageBucket: "gs://nifty-master-464ff.appspot.com",
        messagingSenderId: "1006947390366",
        appId: "1:1006947390366:android:36b49bf8bf3ed54cfcbc8f",
        measurementId: "YOUR MEASUREMENT ID"
};

firebase.initializeApp(firebaseConfig);
const messaging=firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log(payload);
    const notification=JSON.parse(payload);
    const notificationOption={
        body:notification.body,
        icon:notification.icon
    };
    return self.registration.showNotification(payload.notification.title,notificationOption);
});