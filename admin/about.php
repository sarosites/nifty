<!DOCTYPE html>
<html lang="en">
<head>
  <title>Nifity</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body> 
 
<div class="container">
  <h2>About Us</h2>
  <!-- <div class="card-columns"> -->
  <div class="row">
    <div class="col-md-6">
      <div class="card bg-warning">
      <div class="card-body text-center">
         <h2 style="color: white;">About Us</h2>
        <p class="card-text">Nifty Chacha is a product of Nifty Master team of experts and professionals who are highly qualified in the area of fundamental and technical analysis. The team has vast experience over 20 years across the asset classes Viz. Equities, Currencies and commodities. Our professionals are experts in fund management and generating good returns for our clients over years. We provide Research Services in Nifty, Bank Nifty, stock futures and Commodities Viz. Gold, Silver and crude at nominal price"</p>
      </div>
    </div>
    </div>
    <div class="col-md-3">
      <div class="card bg-info">
      <div class="card-body text-center">
        <h2 style="color: white;">Mission</h2>
        <p class="card-text">Our mission is to generate wealth to the individual investors across different asset classes and grow as the finest advisory firm in the industry."
          <br><br><br></p>
      </div>
    </div>
    </div>
    <div class="col-md-3">
      <div class="card bg-danger">
      <div class="card-body text-center">
        <h2 style="color: white;">Vision</h2>
        <p class="card-text"> Our Vision is to grow as a best research and advisory firm by generating great returns to our clients.
          <br><br><br><br><br></p>
      </div>
    </div>  
    </div>
  </div>
</div>
</div>

</body>
</html>
