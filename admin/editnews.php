<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['username']))
{
echo "<script>window.location.href='index.php'</script>";

}
else
{
include"header.php";

include("db.php");

  if(isset($_GET['uid']))
  {
    $id=$_GET['uid'];
    $editproject=mysqli_query($conn,"SELECT * FROM newss WHERE id='".$id."'");
    $row=mysqli_fetch_array($editproject);
  }
// start compress

 function compressImage($source, $destination, $quality) { 
    // Get image info 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
     
    // Create a new image from file 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    // Save image 
    imagejpeg($image, $destination, $quality); 
     
    // Return compressed image 
    return $destination; 
} 
 
 
// File upload path 
$uploadPath = "gallery/"; 
 
// If file upload form is submitted 
$status = $statusMsg = ''; 
if(isset($_POST['pillarsub']) && isset($_POST['pid']))
{
  $id=$_POST['pid'];
  $pillarcat=$_POST['package_id'];
   $name=$_POST['name'];
  $des=$_POST['des'];


  
    $status = 'error'; 
 $query =mysqli_query($conn,"UPDATE newss SET package_id='$pillarcat', name='$name',des='$des' WHERE id='$id'");   
  if(!empty($_FILES["file"]["name"])) { 
        // File info 
        $fileName = basename($_FILES["file"]["name"]); 
        $imageUploadPath = $uploadPath . $fileName; 
        $fileType = pathinfo($imageUploadPath, PATHINFO_EXTENSION); 
         
        // Allow certain file formats 
        $allowTypes = array('jpg','png','jpeg','gif'); 
        if(in_array($fileType, $allowTypes)){ 
            // Image temp source 
            $imageTemp = $_FILES["file"]["tmp_name"]; 
            $imageTemp1 = $_FILES["file"]["name"];
             
            // Compress size and upload image 
            $compressedImage = compressImage($imageTemp, $imageUploadPath, 40); 
             
            if($compressedImage){ 
                $status = 'success'; 
                $statusMsg = "Image compressed successfully."; 
                $sql=mysqli_query($conn,"UPDATE newss SET package_id='$pillarcat',image='$imageTemp1',name='$name',des='$des' WHERE id='$id'");

              
                
            }else{ 
                $statusMsg = "Image compress failed!"; 
               $query =mysqli_query($conn,"UPDATE newss SET package_id='$pillarcat',name='$name',des='$des' WHERE id='$id'");   
            } 
        }else{ 
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
            $query =mysqli_query($conn,"UPDATE newss SET package_id='$pillarcat',name='$name',des='$des' WHERE id='$id'");   
              
        } 
    }else{ 
        $statusMsg = 'Please select an image file to upload.'; 
       $query =mysqli_query($conn,"UPDATE newss SET package_id='$pillarcat', name='$name',des='$des' WHERE id='$id'");   
    } 
echo  "<script>alert('Newss Updated Successfully')</script>"; 
  echo "<script>window.location.href='news.php'</script>";
}


// end compress

?>
<script type="text/javascript">
  function validateForm()
{
  
  var messageLength = CKEDITOR.instances['editor1'].getData().replace(/<[^>]*>/gi, '').length;
    
            if( !messageLength ) {
                document.getElementById('error_productdescription').innerHTML = " Please fill out this fields";
            return false;
            }
      else
      {
        document.getElementById('error_productdescription').innerHTML = "";
      }
        return true;
    }
</script>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header card">
<div class="card-block">
<h5 class="m-b-10">News Management</h5>
<!-- <p class="text-muted m-b-10">HTML5 buttons provides the local file saving features</p>
 --><ul class="breadcrumb-title b-t-default p-t-10">
<li class="breadcrumb-item">
<a href="dashboard.php"> <i class="ti-home"></i> </a>
</li>
<li class="breadcrumb-item"><a href="news.php">News</a>
</li>
<li class="breadcrumb-item"><a href="#!">Edit news</a>
  </li>
</ul>
</div>
</div>


<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<h5>Edit News</h5>
<!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">
<!-- <h4 class="sub-title">Basic Inputs</h4> -->
<form action="" method="post"  enctype="multipart/form-data" onSubmit="return validateForm();">
  <input type="hidden" class="form-control" name="pid" value="<?php echo isset($row['id'])?$row['id']:""; ?>" >


<div class="form-group row">
<label class="col-sm-2 col-form-label">Package Category <b style="color: red">*</b></label>
<div class="col-sm-10">
<select class="form-control" name="package_id" required="">
  <?php
  
    $pillar=mysqli_query($conn,"SELECT * FROM package WHERE id='".$row['package_id']."'");
    $rowpillar=mysqli_fetch_array($pillar);
    if(!empty($rowpillar))
    {
    echo "<option value='".$rowpillar['id']."'>".$rowpillar['planname']."</option>";
    }
  
  
  ?>
  <option value="0">Select Package</option>
  <?php
  
  $pillarcat=mysqli_query($conn,"SELECT * FROM package WHERE status=0 AND del=0");
  while ($rowcat=mysqli_fetch_array($pillarcat)) {
    echo "<option value='".$rowcat['id']."'>".$rowcat['planname']."</option>";
  }
  ?>
</select>
</div>
</div>
 
<div class="form-group row">
<label class="col-sm-2 col-form-label">Image <b style="color: red">*</b></label>
<div class="col-sm-10">
<input type="file" class="form-control" name="file">
 <img  src="gallery/<?php echo isset($row['image'])?$row['image']:""; ?>" alt="img" width="100" required="">
</div>
</div>

<div class="form-group row">
<label class="col-sm-2 col-form-label">Name <b style="color: red">*</b></label>
<div class="col-sm-10">
<input type="text" class="form-control" name="name" value="<?php echo isset($row['name'])?$row['name']:""; ?>" placeholder="Name" required="">
</div>
</div>

<div class="form-group row">
<label class="col-sm-2 col-form-label">Descrption<b style="color: red">*</b></label>
<div class="col-sm-10">
<textarea class="form-control" name="des" value="<?php echo isset($row['des'])?$row['des']:""; ?>" placeholder="Name" required="" ><?php echo isset($row['des'])?$row['des']:""; ?></textarea>
</div>
</div>
<br>
<div class="form-group row">
<div class="col-sm-4">
</div>
<div class="col-sm-4">
<input type="submit" class="form-control btn btn-info" name="pillarsub" value="Submit" style="border-radius: 12px;">
</div>
<div class="col-sm-4">
</div>
</div>
<br/>
<div class="form-group row">
<div class="col-sm-4">
</div>
<div class="col-sm-4">
<a  href="news.php" class="form-control btn btn-info"  style="border-radius: 12px;color:white;">Back</a>
</div>
<div class="col-sm-4">
</div>
</div>
</form>

</div>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
        CKEDITOR.replace( 'editor1' );
        /* $("form").submit( function(e) {
            var messageLength = CKEDITOR.instances['editor'].getData().replace(/<[^>]*>/gi, '').length;
            if( !messageLength ) {
                alert( 'Please enter a message' );
                e.preventDefault();
            }
        }); */
    </script>

<script type="text/javascript">
  
  var messageLength = CKEDITOR.instances['editor1'].getData().replace(/<[^>]*>/gi, '').length;
    
            if( !messageLength ) {
                document.getElementById('error_productdescription').innerHTML = "Please Enter Product Description";
            return false;
            }
      else
      {
        document.getElementById('error_productdescription').innerHTML = "";
      }
</script>

<?php
include"footer.php";
}
?>