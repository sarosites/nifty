<?php
include("db.php");
?>
<html>   
  <head>   
    <title>Pagination</title>   
    <link rel="stylesheet"  
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">   
    <style>   
    table {  
        border-collapse: collapse;  
    }  
        .inline{   
            display: inline-block;   
            float: right;   
            margin: 20px 0px;   
        }   
         
        input, button{   
            height: 34px;   
        }   
  
    .pagination {   
        display: inline-block;   
    }   
    .pagination a {   
        font-weight:bold;   
        font-size:18px;   
        color: black;   
        float: left;   
        padding: 8px 16px;   
        text-decoration: none;   
        border:1px solid black;   
    }   
    .pagination a.active {   
            background-color: pink;   
    }   
    .pagination a:hover:not(.active) {   
        background-color: skyblue;   
    }   
        </style>   
  </head>   
  <body>   
  <center>  
    <?php  
      
    // Import the file where we defined the connection to Database.     
         
    
        $per_page_record = 4;  // Number of entries to show in a page.   
        // Look for a GET variable page if not found default is 1.        
        if (isset($_GET["page"])) {    
            $page  = $_GET["page"];    
        }    
        else {    
          $page=1;    
        }    
    
        $start_from = ($page-1) * $per_page_record;     
    
        $query = "SELECT * FROM users LIMIT $start_from, $per_page_record";     
        $rs_result = mysqli_query ($conn, $query);    
    ?>    
  
    <div class="container">   
      <br>   
      <div>   
        <h1>Pagination Simple Example</h1>   
        <p>This page demonstrates the basic    
           Pagination using PHP and MySQL.   
        </p>   
        <table class="table table-striped table-condensed    
                                          table-bordered">   
          <thead>   
            <tr>   
               
             <th>Sno</th>
<th>Name</th>
<th>Package Name</th>
<!-- <th>Password</th> -->
<th>Email</th>
<th>Age</th>
<th>Phone</th>
<th>City</th>
<th>Pincode</th>
<th>subcription Date</th>
<th>Expire Date</th>
<th>last_Info</th>
            </tr>   
          </thead>   
          <tbody>   
    <?php     
            while ($row = mysqli_fetch_array($rs_result)) {    
                  // Display each field of the records.  
                  $cnt=1;
                while($row=mysqli_fetch_array($rs_result))
                {
                  $pillar1=mysqli_query($conn,"SELECT * FROM package WHERE id='".$row['package_id']."'");
    $rowpillar=mysqli_fetch_array($pillar1);

                ?>
                              <tr>
                              <td><?php echo $cnt;?></td>
                                  <td><?php echo $row['username'];?></td>
                                  <td><?php echo $rowpillar['planname'];?></td>

                                  <!-- <td><?php echo $row['password'];?></td> -->
                                 <td><?php echo $row['email'];?></td>
                                  <td><?php echo $row['age'];?></td>
                                  <td><?php echo $row['phonenumber'];?></td>
                                  <td><?php echo $row['city'];?></td>
                                  <td><?php echo $row['pincode'];?></td>
                                  <td><?php echo date('d-m-Y',strtotime($row['sub_date']));?></td>
                                  <td><?php echo date('d-m-Y',strtotime($row['exp_date']));?></td>
                                  <td><?php echo $row['last_info'];?></td>  
                                </tr>
                                                    
            <?php 
             $cnt=$cnt+1;    
                };    
            ?>     
          </tbody>   
        </table>   
  
     <div class="pagination">    
      <?php  
        $query = "SELECT COUNT(*) FROM users";     
        $rs_result = mysqli_query($conn, $query);     
        $row = mysqli_fetch_row($rs_result);     
        $total_records = $row[0];     
          
    echo "</br>";     
        // Number of pages required.   
        $total_pages = ceil($total_records / $per_page_record);     
        $pagLink = "";       
      
        if($page>=2){   
            echo "<a href='pagination1.php?page=".($page-1)."'>  Prev </a>";   
        }       
                   
        // for ($i=1; $i<=$total_pages; $i++) {   
          if ($page>=1) {   
            $pagepre=$page-1;
            $pagenext=$page+1;
            if($pagepre){
              $pagLink .= "<a href='pagination1.php?page=".$pagepre."'>   
                                                ".$pagepre." </a>";
            }
              
            
            
              $pagLink .= "<a class = 'active' href='pagination1.php?page=".$page."'>".$page." </a>";   
               if($total_pages>=$pagenext) {
                
              $pagLink .= "<a href='pagination1.php?page=".$pagenext."'>   
                                                ".$pagenext." </a>";  
               }                                
          }               
          else  {   
              $pagLink .= "<a href='pagination1.php?page=".$page."'>   
                                                ".$page." </a>";     
          }   
        // };     
        echo $pagLink;   
  
        if($page<$total_pages){   
            echo "<a href='pagination1.php?page=".($page+1)."'>  Next </a>";   
        }   
  
      ?>    
      </div>  
  
  
      <div class="inline">   
      <input id="page" type="number" min="1" max="<?php echo $total_pages?>"   
      placeholder="<?php echo $page."/".$total_pages; ?>" required>   
      <button onClick="go2Page();">Go</button>   
     </div>    
    </div>   
  </div>  
</center>   
  <script>   
    function go2Page()   
    {   
        var page = document.getElementById("page").value;   
        page = ((page><?php echo $total_pages; ?>)?<?php echo $total_pages; ?>:((page<1)?1:page));   
        window.location.href = 'pagination1.php?page='+page;   
    }   
  </script>  
  </body>   
</html> 
<?php
}
?> 