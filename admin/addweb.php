<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['username']))
{
echo "<script>window.location.href='index.php'</script>";

}
else
{

include"header.php";

include("db.php");
// include("image_helper.php");
  function compressImage($source, $destination, $quality) { 
    // Get image info 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
     
    // Create a new image from file 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break;
        case 'image/jpg': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    // Save image 
    imagejpeg($image, $destination, $quality); 
     
    // Return compressed image 
    return $destination; 
} 
 
 
// File upload path 
$uploadPath = "gallery/";
  
// $pillar=mysqli_query($conn,"SELECT * FROM pillar WHERE fld_delete=0");
// $num=mysqli_fetch_array($ret);
if(isset($_POST['pillarsub']))
{
  $packid=$_POST['package_id'];
  $title=$_POST['title'];
  
  $description=$_POST['des'];

 
  // upload code

$name1= $_FILES['file']['name'];

$tmp_name= $_FILES['file']['tmp_name'];

$position= strpos($name1, ".");

$fileextension= substr($name1, $position + 1);

$fileextension= strtolower($fileextension);


if (isset($name1)) {

$path= 'gallery/';

if (empty($name1))
{

echo "Please choose a file";
}

else if (!empty($name1)){

if (($fileextension !== "mp4") && ($fileextension !== "mkv") && ($fileextension !== "ogg") && ($fileextension !== "webm")&& ($fileextension !== "jpg")&& ($fileextension !== "jpeg")&& ($fileextension !== "png")&& ($fileextension !== "pdf"))
{
echo "The file extension must be .mp4, .ogg, or .webm in order to be uploaded";
}


else if (($fileextension == "mp4") || ($fileextension == "mkv")|| ($fileextension == "ogg") || ($fileextension == "webm")|| ($fileextension == "jpg")|| ($fileextension == "jpeg")|| ($fileextension == "png")|| ($fileextension == "pdf"))
{
if (move_uploaded_file($tmp_name, $path.$name1)) {
 if(!empty($_FILES["image"]["name"])) { 
        // File info 
        $fileName = basename($_FILES["image"]["name"]); 
        $imageUploadPath = $uploadPath . $fileName; 
        $fileType = pathinfo($imageUploadPath, PATHINFO_EXTENSION); 
         
        // Allow certain file formats 
        $allowTypes = array('jpg','png','jpeg','gif'); 
        if(in_array($fileType, $allowTypes)){ 
            // Image temp source 
            $imageTemp = $_FILES["image"]["tmp_name"]; 
            $imageTemp1 = $_FILES["image"]["name"];
             
            // Compress size and upload image 
            $compressedImage = compressImage($imageTemp, $imageUploadPath, 40); 
             
            if($compressedImage){ 
                $status = 'success'; 
                $statusMsg = "Image compressed successfully."; 
                // $img=$uploadPath.$imageTemp1;
                $img="https://sr-mediatech.com/nifty%20master/nifty/nifty/admin/".$uploadPath.$imageTemp1;


   $sql=mysqli_query($conn,"INSERT INTO web (package_id,title,description,vlink,image) VALUES ('".$packid."','".$title."','".$description."','".$gallery."','".$img."')");
echo 'Uploaded!';
}else{ 
                $statusMsg = "Image compress failed!"; 
            } 
        }else{ 
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
        } 
    }else{ 
        $statusMsg = 'Please select an image file to upload.'; 
    }

}
}
}
 
}

  
  $gallery=$path.$name1;
  if ($gallery > 50) {
   echo  "<script>alert('large')</script>";
}

    
    

  
  // end

                   

            
    echo  "<script>alert('Webinar Added Successfully')</script>";    
       
 
  echo "<script>window.location.href='web.php'</script>";
}

?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header card">
<div class="card-block">
<h5 class="m-b-10">Classes Management</h5>
<!-- <p class="text-muted m-b-10">HTML5 buttons provides the local file saving features</p>
 --><ul class="breadcrumb-title b-t-default p-t-10">
<li class="breadcrumb-item">
<a href="dashboard.php"> <i class="ti-home"></i> </a>
</li>
<li class="breadcrumb-item"><a href="web.php">Classes</a>
</li>
<li class="breadcrumb-item"><a href="#!"> Add Classes</a>
  </li>
</ul>
</div>
</div>


<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<h5>Add Classes</h5>
<!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">
<!-- <h4 class="sub-title">Basic Inputs</h4> -->
<form action="" method="post"id="subform"  enctype="multipart/form-data">

<div class="form-group row">
<label class="col-sm-2 col-form-label">Package Category <b style="color: red">*</b></label>
<div class="col-sm-10">
<select class="form-control" name="package_id" required="">
  <option value="">Select Package</option>
  <?php
  $pillarcat=mysqli_query($conn,"SELECT * FROM package WHERE status=0 AND del=0");
  while ($rowcat=mysqli_fetch_array($pillarcat)) {
    echo "<option value='".$rowcat['id']."'>".$rowcat['planname']."</option>";
  }
  ?>
</select>
</div>
</div>
<div class="form-group row">
<label class="col-sm-2 col-form-label">Title <b style="color: red">*</b></label>
<div class="col-sm-10">
<input type="text" class="form-control" name="title" placeholder="Title" required="">
</div>
</div>

 <div class="form-group row">
<label class="col-sm-2 col-form-label">Description <b style="color: red">*</b></label>
<div class="col-sm-10">
<!-- <textarea class="form-control" id="editor1" name="reportdescription"   maxlength="1200"></textarea>
 -->
   <span id="error_productdescription" style="color:red;align:center;font-weight:900;font-size:16px;"></span>
                
                <!--<textarea name="editor1"></textarea>-->
                  <textarea class="form-control" id="des" name="des"  required=""  maxlength="1200"></textarea>
 </div>
</div>
<div class="form-group row">
<label class="col-sm-2 col-form-label"> Classes Video</label>
<div class="col-sm-10">
<input type="file" class="form-control" name="file" size="100" required="" onchange="uploadFile()">
<br><progress id="progressBar" value="0" max="100" style="width:300px;"></progress>
  <h3 id="status"></h3>
  <p id="loaded_n_total"></p>
</div>
</div>
<br>
<div class="form-group row">
<label class="col-sm-2 col-form-label"> Images</label>
<div class="col-sm-10">
<input type="file" class="form-control" name="image" size="100" required="">
</div>
</div>
<br>
<div class="form-group row">
<div class="col-sm-4">
</div>
<div class="col-sm-4">
<input type="submit" class="form-control btn btn-info" name="pillarsub" value="Submit" style="border-radius: 12px;">
</div>
<div class="col-sm-4">
</div>
</div>
</form>

</div>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
function _(el) {
  return document.getElementById(el);
}

function uploadFile() {
  var file = _("file").files[0];
  // alert(file.name+" | "+file.size+" | "+file.type);
  var formdata = new FormData();
  formdata.append("file", file);
  var ajax = new XMLHttpRequest();
  ajax.upload.addEventListener("progress", progressHandler, false);
  ajax.addEventListener("load", completeHandler, false);
  ajax.addEventListener("error", errorHandler, false);
  ajax.addEventListener("abort", abortHandler, false);
  ajax.open("POST", "addweb.php");
  ajax.send(formdata);
}

function progressHandler(event) {
  _("loaded_n_total").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
  var percent = (event.loaded / event.total) * 100;
  _("progressBar").value = Math.round(percent);
  _("status").innerHTML = Math.round(percent) + "% uploaded... please wait";
}

function completeHandler(event) {
  _("status").innerHTML = event.target.responseText;
  _("progressBar").value = 0;
}

function errorHandler(event) {
  _("status").innerHTML = "Upload Failed";
}

function abortHandler(event) {
  _("status").innerHTML = "Upload Aborted";
}
</script>

<script>
        CKEDITOR.replace( 'editor1' );
        /* $("form").submit( function(e) {
            var messageLength = CKEDITOR.instances['editor'].getData().replace(/<[^>]*>/gi, '').length;
            if( !messageLength ) {
                alert( 'Please enter a message' );
                e.preventDefault();
            }
        }); */
    </script>

<script type="text/javascript">
  
  var messageLength = CKEDITOR.instances['editor1'].getData().replace(/<[^>]*>/gi, '').length;
    
            if( !messageLength ) {
                document.getElementById('error_productdescription').innerHTML = "Please Enter Product Description";
            return false;
            }
      else
      {
        document.getElementById('error_productdescription').innerHTML = "";
      }
</script>


<?php
include"footer.php";
}
?>