<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['username']))
{
echo "<script>window.location.href='index.php'</script>";

}
else
{

include"header.php";

include("db.php");
if(isset($_GET['delid']))
{

$id=$_GET['delid'];

$sql=mysqli_query($conn,"UPDATE stock SET del=1 WHERE id='$id'");
echo "<script>window.location.href='stock.php'</script>";
}
 
 
 
$pillar=mysqli_query($conn,"SELECT * FROM stock WHERE del=0 ORDER BY id DESC");
// $num=mysqli_fetch_array($ret);

?>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header card">
<div class="card-block">
<h5 class="m-b-10">Stock Management</h5>
<!-- <p class="text-muted m-b-10">HTML5 buttons provides the local file saving features</p>
 --><ul class="breadcrumb-title b-t-default p-t-10">
<li class="breadcrumb-item">
<a href="dashboard.php"> <i class="ti-home"></i> </a>
</li>
<li class="breadcrumb-item"><a href="stock.php">Stock</a>
</li>
</ul>
</div>
</div>


<div class="page-header card">
<div class="card-block">
<h5 class="m-b-10">Filter</h5>
<form action="stock.php" method="post">
<div class="row">
  <div class="col-md-3">
    <input type="text" placeholder="Search Name" class="form-control" name="searchname">
  </div>
  <div class="col-md-3">
    <input type="date"  class="form-control" name="searchdate">

  </div>
  <div class="col-md-3">
    <select class="form-control" name="searchcat" required="">
  <option value="">Select Category</option>
  <option value="buy">Buy</option>
  <option value="sell">Sell</option>

</select>

  </div>
  <div class="col-md-3">
    <select class="form-control" name="searchpack" required="">
  <option value="">Select Package</option>
  <?php
  $pillarcat=mysqli_query($conn,"SELECT * FROM package WHERE status=0 AND del=0");
  while ($rowcat=mysqli_fetch_array($pillarcat)) {
    echo "<option value='".$rowcat['id']."'>".$rowcat['planname']."</option>";
  }
  ?>
</select> 
  </div> 

</div>
<div class="row">
  <div class="col-md-3">
    <button class="btn btn-warning btn-md" name="search">Search</button> 
  </div>
</div>
</form>
</div>
</div>

<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header table-card-header">
<h5>
<a href="addstockfcm.php" class="btn btn-inverse btn-outline-inverse"><i class="ti-layout"></i> ADD STOCK</a></h5>
</div>
<div class="card-block">
<div class="dt-responsive table-responsive">
<table id="basic-btn" class="table table-striped table-bordered nowrap">
<thead>
<tr>
<th>Sno</th>
<th>Name</th>
<th>Package</th>
<th>Category</th>
<th>Rate</th>
<th>Status</th>
<th>Action</th>
</tr>
</thead>
<tbody>
<!-- <tr>
<td>Tiger Nixon</td>
<td>System Architect</td>
<td>Edinburgh</td>
<td>61</td>
<td>2011/04/25</td>
<td>$320,800</td>
</tr> -->
<?php
$cnt=1;

                while($row=mysqli_fetch_array($pillar))
                {
                  $a=array();
                  $b=explode(',',$row['package_id']);
                  // print_r($b);exit;

                  for($i=0;$i<count($b);$i++)
  {
    
    $ss=mysqli_query($conn,"SELECT * FROM package WHERE status=0 AND del=0 AND id='".$b[$i]."' ");
    $rr=mysqli_fetch_array($ss);
    array_push($a, $rr['planname']);
  }
// print_r($a);exit;
    
                  ?>
                              <tr>
                              <td><?php echo $cnt;?></td>
                              <td><?php echo $row['name'];?></td>
                                  <td><?php echo implode(',', $a);?></td>
                                  <td><?php echo $row['category'];?></td>
                                  <td><?php echo $row['rate'];?></td>
                                  <td><i data="<?php echo $row['id'];?>" class="status_checks btn
  <?php echo ($row['status'])?
  'btn-danger': 'btn-success'?>"><?php echo ($row['status'])? 'Inactive' : 'Active'?>
 </i></td>  

                                  <td>
                                      <a href="viewstock.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-secondary btn-sm"><i class="ti-eye"></i></button></a>
                                     <a href="editstock.php?uid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-primary btn-sm"><i class="ti-pencil"></i></button></a>
                                     <a href="stock.php?delid=<?php echo $row['id'];?>"> 
                                     <button class="btn btn-danger btn-sm" onClick="return confirm('Do you really want to delete');"><i class="ti-trash "></i></button></a>
                                  </td>
                              </tr>
                              <?php $cnt=$cnt+1; }?>

</tbody>
</table>
</div>
</div>
</div>




</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
$(document).on('click','.status_checks',function(){
      var status = ($(this).hasClass("btn-success")) ? '1' : '0';
      var msg = (status=='0')? 'Activate' : 'Deactivate';
      if(confirm("Are you sure to "+ msg)){
        var current_element = $(this);
        url = "ajax.php";
        $.ajax({
          type:"POST",
          url: "ajax.php",
          data: {pillarid:$(current_element).attr('data'),status:status},
          success: function(data)
          {   
            location.reload();
          }
        });
      }      
    });

</script>

<?php
include"footer.php";
}
?>