<?php 
 include "db.php";
$sql = mysqli_query($conn,"SELECT * FROM version where del=0");
// $numrows = mysqli_num_rows($sql);
$obj = mysqli_fetch_array($sql);
$return["androidversion"] =$obj['androidversion'];
$return["androidlink"] = $obj['androidapplink'];
$return["iosversion"] = $obj['iosversion'];
$return["ioslink"] = $obj['iosapplink'];

mysqli_close($conn);

header('Content-Type: application/json');
 
echo json_encode($return,JSON_UNESCAPED_SLASHES);
   
?>