<?php
session_start();
date_default_timezone_set("Asia/Kolkata");
include "db.php";
$sql=mysqli_query($conn,"SELECT * FROM donate_monthly WHERE fld_delete=0")
?>
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from html.codedthemes.com/gradient-able/default/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 15 Aug 2020 07:16:29 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<title>Nifty Master </title>


<!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="Gradient Able Bootstrap admin template made using Bootstrap 4 and it has huge amount of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
<meta name="keywords" content="bootstrap, bootstrap admin template, admin theme, admin dashboard, dashboard template, admin template, responsive" />
<meta name="author" content="codedthemes" />

<!-- <link rel="icon" href="../files/assets/logo2.png" type="image/x-icon"> -->

 	<link rel="icon" href="../files/assets/images/logo1.png" type="image/jpg" />


<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../files/bower_components/bootstrap/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="../files/assets/icon/themify-icons/themify-icons.css">

<link rel="stylesheet" type="text/css" href="../files/assets/icon/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" type="text/css" href="../files/bower_components/switchery/css/switchery.min.css">

<link rel="stylesheet" type="text/css" href="../files/bower_components/bootstrap-tagsinput/css/bootstrap-tagsinput.css" />
<link rel="stylesheet" type="text/css" href="../files/assets/icon/icofont/css/icofont.css">


<link rel="stylesheet" type="text/css" href="../files/assets/css/jquery.mCustomScrollbar.css">

<link rel="stylesheet" href="../files/assets/pages/chart/radial/css/radial.css" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/data-table/extensions/buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../files/assets/pages/clndr-calendar/css/clndr.css">

<script src="../files/assets/pages/chart/echarts/js/echarts-all.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<link rel="stylesheet" type="text/css" href="../files/assets/css/style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<link rel="stylesheet" type="text/css" 
    href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<script type="text/javascript" 
    src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
</head>
<body>

<div class="theme-loader">
<div class="loader-track">
<div class="loader-bar"></div>
</div>
</div>

<div id="pcoded" class="pcoded">
<div class="pcoded-overlay-box"></div>
<div class="pcoded-container navbar-wrapper">
<nav class="navbar header-navbar pcoded-header">
<div class="navbar-wrapper">
<div class="navbar-logo">
<a class="mobile-menu" id="mobile-collapse" href="#!">
<i class="ti-menu"></i>
</a>
<div class="mobile-search">
<div class="header-search">
<div class="main-search morphsearch-search">
<div class="input-group">
<span class="input-group-addon search-close"><i class="ti-close"></i></span>
<input type="text" class="form-control" placeholder="Enter Keyword">
<span class="input-group-addon search-btn"><i class="ti-search"></i></span>
</div>
</div>
</div>
</div>
<a href="index.php">
	<h5>NIFTY MASTER</h5>
</a>
<a class="mobile-options">
<i class="ti-more"></i>
</a>
</div>
<div class="navbar-container container-fluid">
<ul class="nav-left">
<li>
<div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
</li>
<li class="header-search">
<div class="main-search morphsearch-search">
<div class="input-group">
<span class="input-group-addon search-close"><i class="ti-close"></i></span>

</div>
</div>
</li>
<li>
<a href="#!" onclick="javascript:toggleFullScreen()">
<i class="ti-fullscreen"></i>
</a>
</li>
</ul>
<ul class="nav-right">
<li class="">
<!--<a href="#!" class="displayChatbox">-->
<!--<i class="ti-comments"></i>-->
<!--<span class="badge bg-c-green"></span>-->
</a>
</li>
<li class="user-profile header-notification">
<a href="#!">
<img src="../files/assets/images/logo1.png" class="" alt="User-Profile-Image">
<span><?php echo $_SESSION['username']; ?></span>
<i class="ti-angle-down"></i>
</a>
<ul class="show-notification profile-notification">
<li>
<a href="viewprofile.php">
<i class="ti-user"></i> Profile
</a>
</li>
<li>
<a href="logout.php">
<i class="ti-layout-sidebar-left"></i> Logout
</a>
</li>
</ul>
</li>
</ul>
</div>
</div>
</nav>

<div id="sidebar" class="users p-chat-user showChat">
<div class="had-container">
<div class="card card_main p-fixed users-main">
<div class="user-box">
<div class="chat-search-box">
<a class="back_friendlist">
<i class="ti-close"></i>
</a>
<div class="right-icon-control">
<input type="text" class="form-control  search-text" placeholder="Search Member" id="search-friends">
<div class="form-icon">
<i class="ti-search"></i>
</div>
</div>
</div>

<div class="main-friend-list">
	
<div class="media userlist-box" data-id="1" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
<a class="media-left" href="#!">
<img class="media-object img-radius img-radius" src="../files/assets/images/99.gif" alt="Generic placeholder image " style="width: 60%; height: 60%;">
</a>
<div class="media-body">
<div class="f-13 chat-header" style="margin-left: -100%;">dddd</div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>

<div class="showChat_inner">
<div class="media chat-inner-header">
<a class="back_chatBox">
<i class="ti-close"></i>  Doe
</a>
</div>
<div class="media chat-messages">
<a class="media-left photo-table" href="#!">
<div class="media-body chat-menu-content">
<div class="">
<p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
<p class="chat-time">8:20 a.m.</p>
</div>
</div>
</div>
<div class="media chat-messages">
<div class="media-body chat-menu-reply">
<div class="">
<p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
<p class="chat-time">8:20 a.m.</p>
</div>
</div>
<div class="media-right photo-table">
<a href="#!">
</a></div>
</div>
</div>
</div>

<div class="pcoded-main-container">
<div class="pcoded-wrapper">
<nav class="pcoded-navbar">
<div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
<div class="pcoded-inner-navbar main-menu">
<div class="pcoded-navigation-label"><img src="../files/assets/images/logo.png" style="width: 40%; margin-left: 19%;"></div>
<ul class="pcoded-item pcoded-left-item">
<li class="">
<a href="dashboard.php">
<span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
<span class="pcoded-mtext">Dashboard</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="poster.php">
<span class="pcoded-micon"><i class="ti-layout"></i><b>P</b></span>
<span class="pcoded-mtext">Poster Management</span>
<span class="pcoded-mcaret"></span>
</a>

</li>
<li class="">
<a href="package.php">
<span class="pcoded-micon"><i class="ti-clipboard"></i><b>P</b></span>
<span class="pcoded-mtext">Package Management</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="youtube.php">
<span class="pcoded-micon"><i class="ti-image"></i><b>G</b></span>
<span class="pcoded-mtext">Youtube Management</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="web.php">
<span class="pcoded-micon"><i class="ti-video-camera"></i><b>V</b></span>
<span class="pcoded-mtext">Classes Management</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="stock.php">
<span class="pcoded-micon"><i class="ti-receipt"></i><b>R</b></span>
<span class="pcoded-mtext">Stock Management</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="news.php">
<span class="pcoded-micon"><i class="ti-bookmark-alt"></i><b>P</b></span>
<span class="pcoded-mtext">News</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="users.php">
<span class="pcoded-micon"><i class="ti-layers-alt"></i><b>P</b></span>
<span class="pcoded-mtext">Users</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="account.php">
<span class="pcoded-micon"><i class="ti-receipt"></i><b>R</b></span>
<span class="pcoded-mtext">Account</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
<li class="">
<a href="common.php">
<span class="pcoded-micon"><i class="ti-receipt"></i><b>R</b></span>
<span class="pcoded-mtext">Notification</span>
<span class="pcoded-mcaret"></span>
</a>
</li>
</ul>


</div>
</nav>