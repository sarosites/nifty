<?php
error_reporting(0);
session_start();
if(!isset($_SESSION['username']))
{
echo "<script>window.location.href='index.php'</script>";

}
else
{

include"header.php";

include("db.php");
// start


  
// $pillar=mysqli_query($conn,"SELECT * FROM pillar WHERE fld_delete=0");
// $num=mysqli_fetch_array($ret);

  function compressImage($source, $destination, $quality) { 
    // Get image info 
    $imgInfo = getimagesize($source); 
    $mime = $imgInfo['mime']; 
     
    // Create a new image from file 
    switch($mime){ 
        case 'image/jpeg': 
            $image = imagecreatefromjpeg($source); 
            break;
        case 'image/jpg': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/png': 
            $image = imagecreatefrompng($source); 
            break; 
        case 'image/gif': 
            $image = imagecreatefromgif($source); 
            break; 
        default: 
            $image = imagecreatefromjpeg($source); 
    } 
     
    // Save image 
    imagejpeg($image, $destination, $quality); 
     
    // Return compressed image 
    return $destination; 
} 
 
 
// File upload path 
$uploadPath = "gallery/project/"; 


if(isset($_POST['pillarsub']))

{
// print_r($_POST);exit;
    
    // $videolink =$_POST['vlink'];
     if(!empty($_POST['vlink'])){
      // $description=$_POST['editor1'];
      $pillarcat=$_POST['pillarcat'];
    $name1=$_POST['videoname'];
      function getYoutubeEmbedUrl($url){
    $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_]+)\??/i';
    $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))(\w+)/i';

    if (preg_match($longUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }

    if (preg_match($shortUrlRegex, $url, $matches)) {
        $youtube_id = $matches[count($matches) - 1];
    }
    return 'https://www.youtube.com/embed/' . $youtube_id ;
}
$url = $_POST['vlink'];
$embeded_url = getYoutubeEmbedUrl($url);
$vurl=isset($embeded_url)?$embeded_url:"NULL";
$status = 'error'; 
    if(!empty($_FILES["file"]["name"])) { 
        // File info 
        $fileName = basename($_FILES["file"]["name"]); 
        $imageUploadPath = $uploadPath . $fileName; 
        $fileType = pathinfo($imageUploadPath, PATHINFO_EXTENSION); 
         
        // Allow certain file formats 
        $allowTypes = array('jpg','png','jpeg','gif'); 
        if(in_array($fileType, $allowTypes)){ 
            // Image temp source 
            $imageTemp = $_FILES["file"]["tmp_name"]; 
            $imageTemp1 = $_FILES["file"]["name"];
             
            // Compress size and upload image 
            $compressedImage = compressImage($imageTemp, $imageUploadPath, 40); 
             
            if($compressedImage){ 
                $status = 'success';
  $sql=mysqli_query($conn,"INSERT INTO youtube (package_id,title,vurl,image) VALUES ('".$pillarcat."','".$name1."','".$vurl."','".$imageTemp1."')");

}else{ 
                $statusMsg = "Image compress failed!"; 
            } 
        }else{ 
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, & GIF files are allowed to upload.'; 
        } 
    }else{ 
        $statusMsg = 'Please select an image file to upload.'; 
    } 
      echo  "<script>alert('Youtube video  Added Successfully')</script>";
  echo "<script>window.location.href='youtube.php'</script>";

} 
 
// Display status message 
echo $statusMsg; 

//end
  }
  


?>
<script type="text/javascript">
  function validateForm()
{
  
  var messageLength = CKEDITOR.instances['editor1'].getData().replace(/<[^>]*>/gi, '').length;
    
            if( !messageLength ) {
                document.getElementById('error_productdescription').innerHTML = " Please fill out this fields";
            return false;
            }
      else
      {
        document.getElementById('error_productdescription').innerHTML = "";
      }
        return true;
    }
</script>
<div class="pcoded-content">
<div class="pcoded-inner-content">

<div class="main-body">
<div class="page-wrapper">

<div class="page-header card">
<div class="card-block">
<h5 class="m-b-10">video Management</h5>
<!-- <p class="text-muted m-b-10">HTML5 buttons provides the local file saving features</p>
 --><ul class="breadcrumb-title b-t-default p-t-10">
<li class="breadcrumb-item">
<a href="dashboard.php"> <i class="ti-home"></i> </a>
</li>
<li class="breadcrumb-item"><a href="video.php">video</a>
</li>
<li class="breadcrumb-item"><a href="#!">Add video</a>
  </li>
</ul>
</div>
</div>


<div class="page-body">
<div class="row">
<div class="col-sm-12">

<div class="card">
<div class="card-header">
<h5>Add video</h5>
<!-- <span>Add class of <code>.form-control</code> with <code>&lt;input&gt;</code> tag</span> -->
</div>
<div class="card-block">
<!-- <h4 class="sub-title">Basic Inputs</h4> -->
<form action="" method="post" enctype="multipart/form-data"  onSubmit="return validateForm();">


<div class="form-group row">
<label class="col-sm-2 col-form-label">Package Category <b style="color: red">*</b></label>
<div class="col-sm-10">
<select class="form-control" name="pillarcat" required="">
  <option value="">Select Package</option>
  <?php
  $pillarcat=mysqli_query($conn,"SELECT * FROM package WHERE status=0 AND del=0");
  while ($rowcat=mysqli_fetch_array($pillarcat)) {
    echo "<option value='".$rowcat['id']."'>".$rowcat['planname']."</option>";
  }
  ?>
</select>
</div>
</div>
<div class="form-group row">
<label class="col-sm-2 col-form-label">Title <b style="color: red">*</b></label>
<div class="col-sm-10">
<input type="text" class="form-control" name="videoname"  placeholder="Name" required="">
</div>
</div>
<div class="form-group row">
<label class="col-sm-2 col-form-label">Video Url <b style="color: red">*</b></label>
<div class="col-sm-10">
<input type="text" class="form-control" name="vlink"  placeholder="Enter Url" required="">
</div>
</div>
<div class="form-group row">
<label class="col-sm-2 col-form-label">Image <b style="color: red">*</b></label>
<div class="col-sm-10">
<input type="file" class="form-control" name="file" required="" >
</div>
</div>
 <!-- 
<div class="form-group row">
<label class="col-sm-2 col-form-label">Video</label>
<div >
<input class="col-sm-4" type="file" class="form-control" name="file" >
<br>
<div class="col-sm-6">
  <img  src="total/gallery/video/<?php echo isset($row['vvideo'])?$row['vvideo']:""; ?>" alt="img" width="200  " >

   <a href="editpillar.php?idd=<?php echo $row['id'];?>&delete_image=<?php echo $row['gallery'];?>">X</a>
</div>
</div> -->
<br>
<div class="form-group row">
<div class="col-sm-4">
</div>
<div class="col-sm-4">
<input type="submit" class="form-control btn btn-info" name="pillarsub" value="Submit" style="border-radius: 12px;">
</div>
<div class="col-sm-4">
</div>
</div>
</form>

</div>
</div>
</div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
        CKEDITOR.replace( 'editor1' );
        /* $("form").submit( function(e) {
            var messageLength = CKEDITOR.instances['editor'].getData().replace(/<[^>]*>/gi, '').length;
            if( !messageLength ) {
                alert( 'Please enter a message' );
                e.preventDefault();
            }
        }); */
    </script>

<script type="text/javascript">
  
  var messageLength = CKEDITOR.instances['editor1'].getData().replace(/<[^>]*>/gi, '').length;
    
            if( !messageLength ) {
                document.getElementById('error_productdescription').innerHTML = "Please Enter Product Description";
            return false;
            }
      else
      {
        document.getElementById('error_productdescription').innerHTML = "";
      }
</script>

<?php
include"footer.php";
}
?>