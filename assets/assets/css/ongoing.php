<?php
include"header.php";

?>

<div class="breadcroumb-area bread-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="breadcroumb-title text-center">
						<h1>Community Development</h1>
						<h6><a href="index.html">Home</a> / Our Works / Pillars / Commnuity Development </h6>
					</div>
				</div>
			</div>
		</div>
	</div>

<div class="blog-area mar-top-50 section-padding">
		<div class="container">
			<div class="row">

				<div class="col-lg-4 col-md-6 mar-bt-50">
					<div class="each-case-2">
					<div class="single-blog-item">
						<div class="blog-bg">
					<img src="assets/img/blog-bg-1.jpg" alt="">

							<div class="blog-content">
								<p class="blog-meta">Posted by <b>Admin</b> on Mar 30, 2020</p>
								<h5><a href="single-blog.php">Right of Natinallity</a></h5>
								<p>The provision of education relating to indigenous horticulture,psum dolor sit </p>
								<a href="#" class="read-more">Read More</a>
							</div>
						</div>
					</div>
				</div>
				</div>

				<div class="col-lg-4 col-md-6">
					<div class="each-case-2">
					<div class="single-blog-item">
						<div class="blog-bg">
							<img src="assets/img/blog-bg-2.jpg" alt="">
							<div class="blog-content">
								<p class="blog-meta">Posted by <b>Admin</b> on Feb 05, 2020</p>
								<h5><a href="single-blog.php">Mentel Heath</a></h5>
								<p>Training and skills development for disadvantaged persons with the purpose of enabling</p>
								<a href="#" class="read-more">Read More</a>
							</div>
						</div>
					</div>
				</div>
				</div>

				<div class="col-lg-4 col-md-6">
					<div class="each-case-2">
					<div class="single-blog-item">
						<div class="blog-bg">
							<img src="assets/img/blog-bg-3.jpg" alt="">
							<div class="blog-content">
								<p class="blog-meta">Posted by <b>Admin</b> on Jan 10, 2020</p>
								<h5><a href="single-blog.php">Careline</a></h5>
								<p>Research, including ecological, educational, social and scientific research on topics related </p>
								<a href="#" class="read-more">Read More</a>
							</div>
						</div>
					</div>
				</div>
				</div>


			
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-6 text-center">
					<a href="blog.html" class="main-btn">More Development Projects</a>
				</div>
				<div class="col-lg-3"></div>
			</div>
		</div>
	</div>

<?php
include"footer.php";

?>